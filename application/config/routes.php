<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'welcome';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route['dashboard'] ='home/Home/index';

$route['user-validation'] = 'validation/User_validation/index';
$route['list-validation'] = 'validation/User_validation/json';
$route['find-data/(:num)'] = 'validation/User_validation/update_data/$1';
$route['reject-data/(:num)'] = 'validation/User_validation/reject_data/$1';

$route['validation-history'] = 'validation/User_validation_history/index';
$route['list-validation-history'] = 'validation/User_validation_history/json';

$route['deposit-balance'] = 'deposit/Deposit_balance/index';
$route['list-deposit-balance'] = 'deposit/Deposit_balance/json';
$route['success-data-balance/(:num)'] = 'deposit/Deposit_balance/process_data/$1';
$route['pending-data-balance/(:num)'] = 'deposit/Deposit_balance/pending_data/$1';
$route['reject-data-balance/(:num)'] = 'deposit/Deposit_balance/reject_data/$1';

$route['deposit-history'] = 'deposit/Deposit_history/index';
$route['list-deposit-history'] = 'deposit/Deposit_history/json';

$route['withdrawal-balance'] = 'withdrawal/Withdrawal_balance/index';
$route['list-withdrawal-balance'] = 'withdrawal/Withdrawal_balance/json';
$route['success-data-withdrawal/(:num)'] = 'withdrawal/Withdrawal_balance/success_data/$1';
$route['pending-data-withdrawal/(:num)'] = 'withdrawal/Withdrawal_balance/pending_data/$1';
$route['reject-data-withdrawal/(:num)'] = 'withdrawal/Withdrawal_balance/reject_data/$1';

$route['withdrawal-history'] = 'withdrawal/Withdrawal_history/index';
$route['list-withdrawal-history'] = 'withdrawal/Withdrawal_history/json';

$route['transfer-balance'] = 'transfer/Transfer_balance/index';
$route['list-transfer-balance'] = 'transfer/Transfer_balance/json';
$route['success-data-transfer/(:num)'] = 'transfer/Transfer_balance/success_data/$1';
$route['pending-data-transfer/(:num)'] = 'transfer/Transfer_balance/pending_data/$1';
$route['reject-data-transfer/(:num)'] = 'transfer/Transfer_balance/reject_data/$1';

$route['transfer-history'] = 'transfer/Transfer_balance_history/index';
$route['list-transfer-history'] = 'transfer/Transfer_balance_history/json';

$route['from-account'] = 'transfer/From_account/index';
$route['list-from-account'] = 'transfer/From_account/json';
$route['success-from-account/(:num)'] = 'transfer/From_account/success_data/$1';
$route['pending-from-account/(:num)'] = 'transfer/From_account/pending_data/$1';
$route['reject-from-account/(:num)'] = 'transfer/From_account/reject_data/$1';

$route['from-account-history'] = 'transfer/From_account_history/index';
$route['list-from-history'] = 'transfer/From_account_history/json';

$route['to-account'] = 'transfer/To_account/index';
$route['list-to-account'] = 'transfer/To_account/json';
$route['success-to-account/(:num)'] = 'transfer/To_account/success_data/$1';
$route['pending-to-account/(:num)'] = 'transfer/To_account/pending_data/$1';
$route['reject-to-account/(:num)'] = 'transfer/To_account/reject_data/$1';

$route['to-account-history'] = 'transfer/To_account_history/index';
$route['list-to-history'] = 'transfer/To_account_history/json';