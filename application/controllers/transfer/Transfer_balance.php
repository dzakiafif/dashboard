<?php 


class Transfer_balance extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->library('twig');
		$this->load->model('transfer/transfer_balance_model','model');
	}

	public function index()
	{
		return $this->twig->display('transfer/transfer-balance');
	}

	public function json()
	{
		$search = $this->input->post('search')['value'];
		$length = $this->input->post('length');
		$start = $this->input->post('start');

		$list = $this->model->getHistory($search,$length,$start);

		$data = [];

		foreach ($list as $transfer_balance) {
			$start++;
			$row = [];
			$row['id'] = $transfer_balance->transfer_balance_id;
			$row['no'] = $start;
			$row['ticket'] = $transfer_balance->ticket;
			$row['form_user'] = $transfer_balance->user_from;
			$row['to_user'] = $transfer_balance->user_to;
			$row['amount'] = $transfer_balance->amount;

			if($transfer_balance->status == 0) {
				$row['open'] = 'open';
			}

			$row['action'] = '<a href="'. site_url('/success-data-transfer/' . $row['id'] .'') .'" class="btn btn-success btn-xs">Processed</a>';

			$data[] = $row;
		}

		$output = [
			'data' => $data
		];

		echo json_encode($output);
	}

	public function success_data($id)
	{
		$transfer = $this->model->get_data_by_id($id);

		$data = $this->model->update_data_by_status($transfer['transfer_balance_id'],1);

		$a = $transfer['amount'];

		$b = $transfer['balance_from'];

		$d = $transfer['balance_to'];

		$c = $b - $a;

		$to = $d + $a;

		$from_balance = $this->model->update_data_by_balance($transfer['from_user_id'],$c);

		$to_balance = $this->model->update_data_by_balance($transfer['to_user_id'],$to);

		redirect('/transfer-balance');

	}

	public function pending_data($id)
	{
		$transfer = $this->model->get_data_by_id($id);

		$data = $this->model->update_data_by_status($transfer['transfer_balance_id'],2);

		redirect('/transfer-balance');
	}

	public function reject_data($id)
	{
		$transfer = $this->model->get_data_by_id($id);

		$data = $this->model->update_data_by_status($transfer['transfer_balance_id'],9);

		redirect('/transfer-balance');
	}


}