<?php 


class From_account extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->library('twig');
		$this->load->model('transfer/from_account_model','model');
	}

	public function index()
	{
		return $this->twig->display('transfer/from-account');
	}

	public function json()
	{
		$search = $this->input->post('search')['value'];
		$length = $this->input->post('length');
		$start = $this->input->post('start');

		$list = $this->model->getHistory($search,$length,$start);

		$data = [];

		foreach ($list as $from_account) {
			$start++;
			$row = [];
			$row['id'] = $from_account->withdrawal;
			$row['no'] = $start;
			$row['ticket'] = $from_account->ticket;
			$row['original_name'] = $from_account->original_name;
			$row['amount'] = $from_account->amount;

			if($from_account->status == 0) {
				$row['open'] = 'open';
			}

			$data[] = $row;
		}

		$output = [
			'data' => $data
		];

		echo json_encode($output);
	}	

	public function success_data($id)
	{
		$data = $this->model->get_data_by_id($id);

		$status = $this->model->update_data_by_status($data['withdrawal'],1);

		$a = $data['balance'];
		$b = $data['amount'];

		$c = $a - $b;

		$balance = $this->model->update_data_by_balance($data['user'],$c);

		redirect('/from-account');
	}

	public function pending_data($id)
	{
		$data = $this->model->get_data_by_id($id);

		$status = $this->model->update_data_by_status($data['withdrawal'],2);

		redirect('/from-account');
	}

	public function reject_data($id)
	{
		$data = $this->model->get_data_by_id($id);

		$status = $this->model->update_data_by_status($data['withdrawal'],9);

		redirect('/from-account');
	}
}