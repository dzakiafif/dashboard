<?php


class To_account_history extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->library('twig');
		$this->load->model('transfer/to_history_model','model');
	}

	public function index()
	{
		return $this->twig->display('transfer/to-account-history');
	}

	public function json()
	{
		$search = $this->input->post('search')['value'];
		$length = $this->input->post('length');
		$start = $this->input->post('start');

		$list = $this->model->getHistory($search,$length,$start);

		$totalData = $this->model->all_to_account_count();

		$totalFiltered = $this->model->countFiltered($search);

		$data = [];

		foreach ($list as $to_account) {
			$start++;
			$row = [];
			$row['id'] = $to_account->deposit;
			$row['no'] = $start;
			$row['ticket'] = $to_account->ticket;
			$row['original_name'] = $to_account->original_name;
			$row['amount'] = $to_account->amount;

			if($to_account->status == 1)
			{
				$row['status'] = 'processed';
			}elseif($to_account->status == 2)
			{
				$row['status'] = 'pending';
			}elseif($to_account->status == 3)
			{
				$row['status'] = 'checked';
			}elseif($to_account->status == 8)
			{
				$row['status'] = 'skipped';
			}else{
				$row['status'] = 'rejected';
			}


			$data[] = $row;
		}

		$output = [
			"recordsTotal" => $totalData,
			"recordsFiltered" => $totalFiltered,
			"data" => $data
		];

		echo json_encode($output);
	}
}