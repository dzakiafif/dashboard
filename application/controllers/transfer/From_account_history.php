<?php 



class From_account_history extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->library('twig');
		$this->load->model('transfer/from_history_model','model');
	}

	public function index()
	{
		return $this->twig->display('transfer/from-account-history');
	}

	public function json()
	{
		$search = $this->input->post('search')['value'];
		$length = $this->input->post('length');
		$start = $this->input->post('start');

		$list = $this->model->getHistory($search,$length,$start);

		$totalData = $this->model->all_from_account_count();

		$totalFiltered = $this->model->countFiltered($search);

		$data = [];

		foreach ($list as $from_account) {
			$start++;
			$row = [];
			$row['id'] = $from_account->withdrawal;
			$row['no'] = $start;
			$row['ticket'] = $from_account->ticket;
			$row['original_name'] = $from_account->original_name;
			$row['amount'] = $from_account->amount;

			if($from_account->status == 1)
			{
				$row['status'] = 'processed';
			}elseif($from_account->status == 2)
			{
				$row['status'] = 'pending';
			}elseif($from_account->status == 3)
			{
				$row['status'] = 'checked';
			}elseif($from_account->status == 8)
			{
				$row['status'] = 'skipped';
			}else{
				$row['status'] = 'rejected';
			}

			$data[] = $row;
		}

		$output = [
			"recordsTotal" => $totalData,
			"recordsFiltered" => $totalFiltered,
			"data" => $data
		];

		echo json_encode($output);
	}
}