<?php 


class Transfer_balance_history extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->library('twig');
		$this->load->model('transfer/transfer_history_model','model');
	}

	public function index()
	{
		return $this->twig->display('transfer/transfer-history');
	}

	public function json()
	{
		$search = $this->input->post('search')['value'];
		$length = $this->input->post('length');
		$start = $this->input->post('start');

		$list = $this->model->getHistory($search,$length,$start);

		$totalData = $this->model->all_transfer_count();

		$totalFiltered = $this->model->countFiltered($search);

		$data = [];

		foreach ($list as $transfer_balance) {
			$start++;
			$row = [];
			$row['id'] = $transfer_balance->transfer_balance_id;
			$row['no'] = $start;
			$row['ticket'] = $transfer_balance->ticket;
			$row['form_user'] = $transfer_balance->user_from;
			$row['to_user'] = $transfer_balance->user_to;
			$row['amount'] = $transfer_balance->amount;

			if($transfer_balance->status == 1)
			{
				$row['status'] = '<span class="label label-success">processed</span>';
			}elseif($transfer_balance->status == 2)
			{
				$row['status'] = '<span class="label label-warning">pending</span>';
			}else{
				$row['status'] = '<span class="label label-danger">rejected</span>';
			}

			$data[] = $row;
		}

		$output = [
			"recordsTotal" => $totalData,
			"recordsFiltered" => $totalFiltered,
			"data" => $data
		];

		echo json_encode($output);

	}
}