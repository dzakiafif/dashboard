<?php 


class To_account extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->library('twig');
		$this->load->model('transfer/to_account_model','model');
	}

	public function index()
	{
		return $this->twig->display('transfer/to-account');
	}

	public function json()
	{
		$search = $this->input->post('search')['value'];
		$length = $this->input->post('length');
		$start = $this->input->post('start');

		$list = $this->model->getHistory($search,$length,$start);

		$data = [];

		foreach ($list as $to_account) {
			$start++;
			$row = [];
			$row['id'] = $to_account->deposit;
			$row['no'] = $start;
			$row['ticket'] = $to_account->ticket;
			$row['original_name'] = $to_account->original_name;
			$row['amount'] = $to_account->amount;

			if($to_account->status == 0) {
				$row['open'] = 'open';
			}

			$data[] = $row;
		}

		$output = [
			'data' => $data
		];

		echo json_encode($output);
	}

	public function success_data($id)
	{
		$data = $this->model->get_data_by_id($id);

		$status = $this->model->update_data_by_status($data['deposit'],1);

		$a = $data['balance'];

		$b = $data['amount'];

		$c = $a + $b;

		$balance = $this->model->update_data_by_balance($data['user'],$c);

		redirect('/to-account');
	}

	public function pending_data($id)
	{
		$data = $this->model->get_data_by_id($id);

		$status = $this->model->update_data_by_status($data['deposit'],2);

		redirect('/to-account');
	}

	public function reject_data($id)
	{
		$data = $this->model->get_data_by_id($id);

		$status = $this->model->update_data_by_status($data['deposit'],9);

		redirect('/to-account');
	}
}