<?php 


class Withdrawal_history extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->library('twig');
		$this->load->model('withdrawal/withdrawal_history_model','model');
	}

	public function index()
	{
		return $this->twig->display('withdrawal/withdrawal-history');
	}

	public function json()
	{
		$search = $this->input->post('search')['value'];
		$length = $this->input->post('length');
		$start = $this->input->post('start');

		$list = $this->model->getHistory($search,$length,$start);

		$totalData = $this->model->all_withdrawal_count();

		$totalFiltered = $this->model->countFiltered($search);

		$data = [];

		foreach ($list as $withdrawal_history) {
			$start++;
			$row = [];
			$row['id'] = $withdrawal_history->withdrawal_balance_id;
			$row['no'] = $start;
			$row['ticket'] = $withdrawal_history->ticket;
			$row['original_name'] = $withdrawal_history->original_name;
			$row['amount'] = $withdrawal_history->amount;

			if($withdrawal_history->status == 1)
			{
				$row['status'] = '<span class="label label-success">processed</span>';
			}elseif($withdrawal_history->status	== 2)
			{
				$row['status'] = '<span class="label label-warning">pending</span>';
			}else {
				$row['status'] = '<span class="label label-danger">rejected</span>';
			}

			$data[] = $row;
		}

		$output = [
			"recordsTotal" => $totalData,
			"recordsFiltered" => $totalFiltered,
			"data" => $data
		];

		echo json_encode($output);
	}
}