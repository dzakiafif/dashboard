<?php 



class Withdrawal_balance extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->library('twig');
		$this->load->model('withdrawal/withdrawal_balance_model','model');
	}

	public function index()
	{
		return $this->twig->display('withdrawal/withdrawal-balance');
	}

	public function json()
	{
		$search = $this->input->post('search')['value'];
		$length = $this->input->post('length');
		$start = $this->input->post('start');

		$list = $this->model->getHistory($search,$length,$start);

		$data = [];

		foreach ($list as $withdrawal_balance) {
			$start++;
			$row = [];
			$row['id'] = $withdrawal_balance->withdrawal_balance_id;
			$row['no'] = $start;
			$row['ticket'] = $withdrawal_balance->ticket;
			$row['original_name'] = $withdrawal_balance->original_name;
			$row['amount'] = $withdrawal_balance->amount;

			if($withdrawal_balance->status == 0) {
				$row['open'] = 'open';
			}

			$data[] = $row;
		}

		$output = [
			'data' => $data
		];

		echo json_encode($output);
	}

	public function success_data($id)
	{
		$withdrawal = $this->model->get_data_by_id($id);

		$data = $this->model->update_data_by_id($withdrawal['withdrawal_balance_id'],1);

		$a = $withdrawal['balance'];

		$b = $withdrawal['amount'];

		$c = $a - $b;

		$balance = $this->model->update_data_balance($withdrawal['user_id'],$c);

		redirect('/withdrawal-balance');
	}

	public function pending_data($id)
	{
		$withdrawal = $this->model->get_data_by_id($id);

		$data = $this->model->update_data_by_id($withdrawal['withdrawal_balance_id'],2);

		redirect('/withdrawal-balance');
	}

	public function reject_data($id)
	{
		$withdrawal = $this->model->get_data_by_id($id);

		$data = $this->model->update_data_by_id($withdrawal['withdrawal_balance_id'],9);

		redirect('/withdrawal-balance');
	}
}