<?php 


class User_validation extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->library('twig');
		$this->load->model('validation/validation_model','model');
	}

	public function index()
	{
		return $this->twig->display('validation/user-validation');
	}

	public function json()
	{
		$search = $this->input->post('search')['value'];
		$length = $this->input->post('length');
		$start = $this->input->post('start');

		$list = $this->model->getHistory($search,$length,$start);

		$totalData = $this->model->all_validation_count();

		$totalFiltered = $this->model->countFiltered($search);

		$data = [];

		foreach ($list as $validation) {
			$start++;
			$row = [];
			$row['id'] = $validation->user_validation_id;
			$row['no'] = $start;
			$row['ticket'] = $validation->ticket;
			$row['bank_beneficiary_name'] = $validation->bank_beneficiary_name;

			$row['created'] = $validation->created_at;
			
			// if($validation->status == 0) {
			// 	$row['open'] = 'open';
			// }

			// $row['reject'] = '<a href="' .site_url('/reject-data/' . $row['id'] .'') . '" class="btn btn-danger btn-xs">Reject</a>';
			
			
			// $row['success'] = '<a href="'. site_url('/find-data/' . $row['id'] .'') .'" class="btn btn-success btn-xs">Processed</a>';

			$data[] = $row;
		}

		$output = [
			"recordsTotal" => intval($totalData),
			"recordsFiltered" => intval($totalFiltered),
			"data" => $data
		];

		echo json_encode($output);
	}

	public function update_data($id)
	{
		$validation = $this->model->find_data_by_id($id);

		$data = $this->model->update_data_by_status($validation['user_validation_id'],1);

		$user = $this->model->update_user_by_id($validation['user_id'],$validation['name'],$validation['phone'],$validation['address'],$validation['postal_code'],$validation['city'],$validation['state'],$validation['country'],$validation['file']);

		redirect('/user-validation');
	}

	public function reject_data($id)
	{
		$validation = $this->model->find_data_by_id($id);

		$data = $this->model->update_data_by_status($validation['user_validation_id'],2);

		redirect('/user-validation');
	}
}