<?php 



class User_validation_history extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->library('twig');
		$this->load->model('validation/validation_history_model','model');
	}

	public function index()
	{
		return $this->twig->display('validation/validation-history');
	}

	public function json()
	{
		$search = $this->input->post('search')['value'];
		$length = $this->input->post('length');
		$start = $this->input->post('start');

		$list = $this->model->getHistory($search,$length,$start);

		$totalData = $this->model->all_validation_count();

		$totalFiltered = $this->model->countFiltered($search);

		$data = [];

		foreach ($list as $validation) {
			$start++;
			$row = [];
			$row['id'] = $validation->user_validation_id;
			$row['no'] = $start;
			$row['ticket'] = $validation->ticket;
			$row['bank_beneficiary_name'] = $validation->bank_beneficiary_name;

			if($validation->status == 1)
			{
				$row['status'] = '<span class="label label-success">processed</span>';
			}elseif($validation->status == 2)
			{
				$row['status'] = '<span class="label label-warning">pending</span>';
			}else{
				$row['status'] = '<span class="label label-danger">rejected</span>';
			}

			$data[] = $row;
		}

		$output = [
			"recordsTotal" => $totalData,
			"recordsFiltered" => $totalFiltered,
			"data" => $data
		];

		echo json_encode($output);
	}
}