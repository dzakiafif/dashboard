<?php 



class Deposit_balance extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->library('twig');
		$this->load->model('deposit/deposit_balance_model','model');
	}

	public function index()
	{
		return $this->twig->display('deposit/deposit-balance');
	}

	public function json()
	{
		$search = $this->input->post('search')['value'];
		$length = $this->input->post('length');
		$start = $this->input->post('start');

		$list = $this->model->getHistory($search,$length,$start);

		$data = [];

		foreach ($list as $deposit) {
			$start++;
			$row = [];
			$row['id'] = $deposit->deposit_balance_id;
			$row['no'] = $start;
			$row['ticket'] = $deposit->ticket;
			$row['original_name'] = $deposit->original_name;
			$row['amount'] = $deposit->amount;

			if($deposit->status == 0)
			{
				$row['open'] = 'open';
			}

			$data[] = $row;
		}

		$output = [
			'data' => $data
		];

		echo json_encode($output);


	}

	public function testing($id)
	{
		$data = $this->model->get_data_by_id($id);

		return var_dump($data);
	}

	public function process_data($id)
	{
		$deposit = $this->model->get_data_by_id($id);

		$data = $this->model->update_data_by_id($deposit['deposit_balance_id'],1);

		$a = $deposit['balance'];
		$b = $deposit['amount'];

		$c = $a + $b;

		$balance = $this->model->update_data_by_balancce($deposit['user_id'],$c);

		redirect('/deposit-balance');
	}

	public function pending_data($id)
	{
		$balance = $this->model->get_data_by_id($id);

		$data = $this->model->update_data_by_id($balance['deposit_balance_id'],2);

		redirect('/deposit-balance');
	}

	public function reject_data($id)
	{
		$balance = $this->model->get_data_by_id($id);

		$data = $this->model->update_data_by_id($balance['deposit_balance_id'],9);

		redirect('/deposit-balance');
	}
}