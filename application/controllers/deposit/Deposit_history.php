<?php 



class Deposit_history extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->library('twig');
		$this->load->model('deposit/deposit_history_model','model');
	}

	public function index()
	{
		return $this->twig->display('deposit/deposit-history');
	}

	public function json()
	{
		$search = $this->input->post('search')['value'];
		$length = $this->input->post('length');
		$start = $this->input->post('start');

		$totalData = $this->model->all_deposit_count();

		$list = $this->model->getHistory($search,$length,$start);

		$totalFiltered = $this->model->countFiltered($search);

		$data = [];

		foreach ($list as $deposit_history) {
			$start++;
			$row = [];
			$row['id'] = $deposit_history->deposit_balance_id;
			$row['no'] = $start;
			$row['ticket'] = $deposit_history->ticket;
			$row['original_name'] = $deposit_history->original_name;
			$row['amount'] = $deposit_history->amount;

			if($deposit_history->status == 1)
			{
				$row['status'] = '<span class="label label-success">process</span>';
			}elseif($deposit_history->status == 2) {
				$row['status'] = '<span class="label label-warning">pending</span>';
			}else{
				$row['status'] = '<span class="label label-danger">rejected</span>';
			}

			$data[] = $row;
		}

		$output = [
			"recordsTotal" => $totalData,
			"recordsFiltered" => $totalFiltered,
			"data" => $data
		];

		echo json_encode($output);
	}
}