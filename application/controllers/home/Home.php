<?php 


class Home extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->library('twig');
	}

	public function index()
	{
		return $this->twig->display('home/home');
	}
}