<?php 



class Deposit_balance_model extends CI_Model
{

	private $_column_search = array(
		'ticket','bank_name','bank_beneficiary_name'
	);

	public function __construct()
	{
		parent::__construct();
	}

	private function _get_datatables_query($search = NULL)
	{
		$this->db->select('tb.*,u.name AS original_name');
		$this->db->from('deposit_balance AS tb');
        $this->db->join('user AS u','tb.user_id = u.user_id');
		$this->db->where('tb.status',0);

		$i = 0;
        foreach ($this->_column_search as $item) { // loop column
            if ($search) { // if datatable send POST for search
                if ($i === 0) { // first loop
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $search);
                } else {
                    $this->db->or_like($item, $search);
                }

                if (count($this->_column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
	}

	 public function getHistory($search = NULL, $start = 0, $length = NULL)
    {
    	$this->_get_datatables_query($search);
        if ($length)
            $this->db->limit($length, $start);
        return $this->db->get()->result();
    }

     public function countFiltered($search = NULL)
    {
        $this->_get_datatables_query($search);
        return $this->db->get()->num_rows();
    }

	public function get_data_by_id($id)
	{
		$this->db->select('ab.*, u.balance AS balance');
		$this->db->from('deposit_balance AS ab');
		$this->db->join('user AS u','u.user_id = ab.user_id');
		$this->db->where('ab.deposit_balance_id',$id);

		return $this->db->get()->row_array();
	}

	public function update_data_by_id($id,$status)
	{
		$this->db->set('status',$status);
		$this->db->where('deposit_balance_id',$id);
		$this->db->update('deposit_balance');

		if ($this->db->affected_rows() == 0)
            return [
                'status' => FALSE,
                'message' => 'server error'
            ];
        return [
            'status' => TRUE
        ];
	}

	public function update_data_by_balancce($id,$balance)
	{
		$this->db->set('balance',$balance);
		$this->db->where('user_id',$id);
		$this->db->update('user');

		if ($this->db->affected_rows() == 0)
            return [
                'status' => FALSE,
                'message' => 'server error'
            ];
        return [
            'status' => TRUE
        ];
	}
}