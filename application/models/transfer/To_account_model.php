<?php 



class To_account_model extends CI_Model
{
	private $_column_search = array(
		'ticket','original_name'
	);

	public function __construct()
	{
		parent::__construct();
	}

	private function _get_datatables_query($search = NULL)
	{
		$this->db->select('tb.ticket,tb.amount, ac.name AS original_name, tb.deposit_transfer_id AS deposit, tb.status');
		$this->db->from('deposit_account AS tb');
		$this->db->join('account AS ac','ac.account_id = tb.account_id');
		$this->db->where('tb.status',0);
		
		$i = 0;
        foreach ($this->_column_search as $item) { // loop column
            if ($search) { // if datatable send POST for search
                if ($i === 0) { // first loop
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $search);
                } else {
                    $this->db->or_like($item, $search);
                }

                if (count($this->_column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
	}

	public function getHistory($search = NULL, $start = 0, $length = NULL)
    {
    	$this->_get_datatables_query($search);
        if ($length)
            $this->db->limit($length, $start);
        return $this->db->get()->result();
    }

    public function countFiltered($search = NULL)
    {
        $this->_get_datatables_query($search);
        return $this->db->get()->num_rows();
    }

	public function get_data_by_id($id)
	{
		$this->db->select('tb.deposit_transfer_id AS deposit,tb.ticket,tb.amount, ac.name AS original_name, u.balance AS balance,u.user_id AS user');
		$this->db->from('deposit_account AS tb');
		$this->db->join('account AS ac','ac.account_id = tb.account_id');
		$this->db->join('user AS u','u.user_id = ac.user_id');
		$this->db->where('tb.deposit_transfer_id',$id);

		return $this->db->get()->row_array();
	}

	public function update_data_by_status($id,$status)
	{
		$this->db->set('status',$status);
		$this->db->where('deposit_transfer_id',$id);
		$this->db->update('deposit_account');

		if ($this->db->affected_rows() == 0)
            return [
                'status' => FALSE,
                'message' => 'server error'
            ];
        return [
            'status' => TRUE
        ];
	}

	public function update_data_by_balance($id,$balance)
	{
		$this->db->set('balance',$balance);
		$this->db->where('user_id',$id);
		$this->db->update('user');

		if ($this->db->affected_rows() == 0)
            return [
                'status' => FALSE,
                'message' => 'server error'
            ];
        return [
            'status' => TRUE
        ];
	}
}