<?php 


class From_history_model extends CI_Model
{
	private $_column_search = array(
		'ticket','original_name'
	);

	public function __construct()
	{
		parent::__construct();
	}

    public function all_from_account_count()
    {
        $query = $this->db->from('withdrawal_account');

        return $query->get()->num_rows();
    }

	private function _get_datatables_query($search = NULL)
	{
		$this->db->select('tb.ticket, tb.amount, ac.name AS original_name, tb.withdrawal_transfer_id AS withdrawal, tb.status');
		$this->db->from('withdrawal_account AS tb');
		$this->db->join('account AS ac','ac.account_id = tb.account_id');
		$this->db->where('tb.status !=',0);

		$i = 0;
        foreach ($this->_column_search as $item) { // loop column
            if ($search) { // if datatable send POST for search
                if ($i === 0) { // first loop
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $search);
                } else {
                    $this->db->or_like($item, $search);
                }

                if (count($this->_column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
	}

	public function getHistory($search = NULL, $start = 0, $length = NULL)
    {
    	$this->_get_datatables_query($search);
        if ($length)
            $this->db->limit($length, $start);
        return $this->db->get()->result();
    }

    public function countFiltered($search = NULL)
    {
        $this->_get_datatables_query($search);
        return $this->db->get()->num_rows();
    }
}